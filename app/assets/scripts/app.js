import "../styles/style.css";

if(module.hot){
    module.hot.accept();
}


$(document).ready(function(){
  $("#services").owlCarousel({
      items:3,
      autoplay:true,
      margin:20,
      loop:true,
      nav:true,
      smartSpeed:1000,
      autoplayHoverPause:true,
      dots:false,
      navText:['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>'],
      
  });
});


$(document).ready(function(){
    $("#isotope-container").magnificPopup({
        delegate:'a',
        type:'image',
        gallery:{
            enabled:true
        },
        zoom:{
            enabled:true,
            duration:300,
            easing:'ease-in-out',
            opener:function(openerElement){
                return openerElement.is('img') ? openerElement:openerElement.find('img');
            }
        }
    })
});
